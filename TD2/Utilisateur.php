<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() : string{
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    public function getPrenom() : string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void {
        $this->prenom = $prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    public function setLogin(string $login): void {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string{
        return nl2br("nom : $this->nom  \n prenom : $this->prenom  \n login :  $this->login");
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau['loginBaseDeDonnees'],$utilisateurFormatTableau['nomBaseDeDonnees'],$utilisateurFormatTableau['prenomBaseDeDonnees']);
    }

    public static function recupererUtilisateurs() : array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        $users = array() ;
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $users[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $users;
    }
}
?>


