<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        $user = $_GET['login'];
        if (isset($user)) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($user);
            if (!isset($utilisateur)) {
                self::afficherVue('../vue/utilisateur/erreur.php');
            }
            else
                self::afficherVue('../vue/utilisateur/detail.php', ["utilisateur" => $utilisateur]);
        }
        else
            self::afficherVue('../vue/utilisateur/erreur.php');

    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire() : void {
        $user = new ModeleUtilisateur($_GET['login'], $_GET['prenom'], $_GET['nom']);
        $user->ajouter();
        ControleurUtilisateur::afficherListe();
    }

}
?>