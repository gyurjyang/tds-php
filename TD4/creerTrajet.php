<!DOCTYPE html>
<html>
<head>
    <title> Exercice 7 </title>
    <meta charset="utf-8" />
</head>

<body>
<?php
require_once "Trajet.php";

$trajet = new Trajet(null, $_POST['depart'], $_POST['arrivee'], new DateTime($_POST['date']), $_POST['prix'],ModeleUtilisateur::recupererUtilisateurParLogin( $_POST['conducteurLogin']), isset($_POST['nonFumeur']), []);
$trajet->ajouter();
echo "Le trajet a bien été ajouté";
?>
</body>
</html>