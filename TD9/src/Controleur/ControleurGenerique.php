<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        $messagesFlash = $_REQUEST["messagesFlash"] ?? [];
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Préférence Controleur", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference() : void {
        $preference = $_REQUEST["controleur_defaut"];
        PreferenceControleur::enregistrer($preference);
        self::afficherVue('preferenceEnregistree.php');
    }

    public static function redirectionVersUrl($url) : void {
        header("Location: $url");
        exit();
    }
}