<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */

        use App\Covoiturage\Lib\ConnexionUtilisateur;

        echo $titre; ?></title>
    <link rel="stylesheet" href="../../TD9/ressources/css/navstyle.css">
</head>
<body>
<header>
    <nav>
            <ul>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/heart.png" alt="coeur"></a>
                </li>
    <?php if (!ConnexionUtilisateur::estConnecte())
    echo '      
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/add-user.png" alt="add"></a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/img/enter.png" alt="login"></a>
                </li>
    ';
        else {
            $loginURL = rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte());
    echo "
                    <li>
                    <a href='controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=$loginURL'> <img src='../ressources/img/user.png' alt='detail'></a>
                </li>
                <li>
                    <a href='controleurFrontal.php?action=deconnecter&controleur=utilisateur'><img src='../ressources/img/logout.png' alt='logout'></a>
                </li>
         ";
        }?>
            </ul>
    </nav>
    <div>
        <?php
        /** @var string[][] $messagesFlash */
        foreach($messagesFlash as $type => $messagesFlashPourUnType) {
            // $type est l'une des valeurs suivantes : "success", "info", "warning", "danger"
            // $messagesFlashPourUnType est la liste des messages flash d'un type
            foreach ($messagesFlashPourUnType as $messageFlash) {
                echo <<< HTML
            <div class="alert alert-$type">
               $messageFlash
            </div>
            HTML;
            }
        }
        ?>
    </div>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Goharik Gyurjyan
    </p>

</footer>
</body>
</html>

