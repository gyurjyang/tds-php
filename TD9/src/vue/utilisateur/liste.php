<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

echo "<h1>Liste des utilisateurs</h1>";
echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<li><p> Utilisateur de login ' . $loginHTML. " " . "<a href='controleurFrontal.php?action=afficherDetail&login=$loginURL&controleur=utilisateur'>(+ d'info)</a>
    </p>";
    if (ConnexionUtilisateur::estAdministrateur(ConnexionUtilisateur::getLoginUtilisateurConnecte())) {
        echo "<a href='controleurFrontal.php?action=afficherFormulaireMiseAJour&login=$loginURL&controleur=utilisateur'>(= modif)</a>";
    }
    echo "</li>";
}
echo "</ol>";