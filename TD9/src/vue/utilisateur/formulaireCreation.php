<?php
use App\Covoiturage\Configuration\ConfigurationSite;
$method = ConfigurationSite::getDebug() ? "get" : "post";
?>
<form method=<?=$method?> action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblanc" name="prenom" id="prenom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : juste" name="nom" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="" placeholder="toto@yopmail.com" name="email" id="email_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <?php
        use App\Covoiturage\Lib\ConnexionUtilisateur;

        if (ConnexionUtilisateur::estAdministrateur(ConnexionUtilisateur::getLoginUtilisateurConnecte()))
            echo "
                    <p class='InputAddOn'>
            <label class='InputAddOn-item' for='estAdmin_id'>Administrateur</label>
            <input class='InputAddOn-field' type='checkbox' placeholder='' name='estAdmin' id='estAdmin_id'>
                    </p>
            "?>
        <p class="InputAddOn">
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>