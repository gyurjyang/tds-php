<?php
namespace App\Covoiturage\Configuration;
class ConfigurationSite {
    public static int $DureeExpirationSession = 3600;

    /**
     * @return int
     */
    public static function getDureeExpirationSession(): int
    {
        return self::$DureeExpirationSession;
    }

    public static function getURLAbsolue() : string
    {
        return "http://localhost/tds-php/TD9/web/controleurFrontal.php";
    }

    public static function getDebug() : bool {
        return true;
    }
}