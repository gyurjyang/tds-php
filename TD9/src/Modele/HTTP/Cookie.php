<?php
namespace App\Covoiturage\Modele\HTTP;
class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        $val = json_encode($valeur);
        isset($dureeExpiration) ? $time = time() + $dureeExpiration : $time = 0;
        setcookie($cle, $val, $time);
    }

    public static function lire(string $cle): mixed
    {
        if (isset($_COOKIE[$cle])) {
            return json_decode($_COOKIE[$cle]);
        }
        return null;
    }

    public static function contient($cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void {
        if (isset($_COOKIE[$cle])) {
            unset($_COOKIE[$cle]);
            setcookie ($cle, "", 1);
        }
    }
}