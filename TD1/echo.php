<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
            $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
            echo $texte;

            /*$prenom = "Marc";

            echo "Bonjour\n " . $prenom;
            echo "Bonjour\n $prenom";
            echo 'Bonjour\n $prenom';

            echo $prenom;
            echo "$prenom";*/

            /*
             * 8.1
            $prenom = "Ver";
            $nom = "Doe";
            $login = "Vdoe";*/

            /*
             8.3
             * $utilisateur = [
                    'prenom' => 'Ver',
                    'nom' => 'Doe',
                    'login' => 'Vdoe'
            ]*/
            $users = array($u1 = [
                'prenom' => 'Ver',
                'nom' => 'Doe',
                'login' => 'Vdoe'
            ],
                $u2 = ['prenom' => 'Ju',
                    'nom' => 'Dorang',
                    'login' => 'Jdorang'
                ],
                $u3 = ['prenom' => 'Coca',
                'nom' => 'Cola',
                'login' => 'Ccola'])
        ?>
        <!--<p> 8.2 <?php //echo "Utilisateur $prenom $nom de login $login" ?></p>-->
        <!-- <p> <?php /*echo "Utilisateur $utilisateur[prenom] $utilisateur[nom] de login $utilisateur[login] ";
            var_dump($utilisateur);*/?></p>-->
    <h1>Liste des utilisateurs : </h1>
    <ul>
        <?php
        if (empty($users)){
            echo 'Il n’y a aucun utilisateur dans le tableau';
        }
        foreach ($users as $indice => $valeur){
            echo "<li>";
            foreach ($valeur as $key => $valeurU){
                echo $valeurU." ";
            }
            echo "</li>";
        }

        ?>
    </ul>
    </body>
</html> 