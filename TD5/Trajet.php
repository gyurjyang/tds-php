<?php

use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\ModeleUtilisateur;

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private ModeleUtilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        ModeleUtilisateur $conducteur,
        bool              $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            ModeleUtilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"]
        );
        $passagers = $trajet->recupererPassagers();
        $trajet->setPassagers($passagers);
        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): ModeleUtilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(ModeleUtilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }



    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        $passagersStr = "";
        if (!empty($this->passagers)) {
            $passagersStr = implode(", ", $this->passagers);
        } else {
            $passagersStr = "Aucun passager";
        }
        return "<p>
        Le trajet $nonFumeur n°$this->id du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee}. 
        Le conducteur est : {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}.";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : bool
    {
        try {
            $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) 
        VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
            // Préparation de la requête
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $values = array(
                "depart" => $this->depart,
                "arrivee" => $this->arrivee,
                "date" => $this->date->format("Y-m-d"),
                "prix" => $this->prix,
                "conducteurLogin" => $this->conducteur->getLogin(),
                "nonFumeur" => $this->nonFumeur ? 1 : 0 //si c'est non fumeur alors c'est 1 sinon c'est 0
            );
            // On donne les valeurs et on exécute la requête
            $pdoStatement->execute($values);
            $id = ConnexionBaseDeDonnees::getPdo()->lastInsertId();
            $this->setId(ConnexionBaseDeDonnees::getPdo()->lastInsertId());
        }
        catch (PDOException $e) {
            return false;
        }
        return true;
    }

    /**
     * @return ModeleUtilisateur[]
     */
    private function recupererPassagers() : array {
        $sql = "SELECT login FROM passager p
                     JOIN utilisateur u ON  p.passagerLogin = u.login
                     WHERE trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "trajetId" => $this->id,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        $lesPassagers = [] ;
        foreach ($pdoStatement as $ligne) {
            $lesPassagers[] = ModeleUtilisateur::recupererUtilisateurParLogin($ligne['login']);
        }
        return $lesPassagers;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $sql = "DELETE FROM passager WHERE passagerLogin = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array("login" => $passagerLogin);
        $pdoStatement->execute($values);
        if ($pdoStatement->rowCount() > 0) {
            return true;
        }
        else return false;
    }

    public static function recupererTrajetParId(string $idTrajet) : ?Trajet
    {
        $sql = "SELECT * from trajet WHERE id = :trajetId";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array("trajetId" => $idTrajet);
        $pdoStatement->execute($values);
        $trajetFormatTableau = $pdoStatement->fetch();
        if (!$trajetFormatTableau) return null;
        else return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
    }
}
