<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        $user = $_GET['login'];
        if (isset($user)) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($user);
            if (!isset($utilisateur)) {
                self::afficherVue('vueGenerale.php', ["titre" => "Page d'erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);  //"redirige" vers la vue
            }
            else
                self::afficherVue('vueGenerale.php', ["titre" => "Détail de $user", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
        }
        else
            self::afficherVue('vueGenerale.php', ["titre" => "Page d'erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);

    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de Création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire() : void {
        $user = new ModeleUtilisateur($_GET['login'], $_GET['prenom'], $_GET['nom']);
        $user->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Créé", "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

}
?>