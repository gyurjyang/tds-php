<?php
namespace App\Covoiturage\Modele;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use Trajet;

class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string{
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    public function getPrenom() : string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void {
        $this->prenom = $prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    public function setLogin(string $login): void {
        $this->login = substr($login, 0, 64);
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null) {
            $this->setTrajetsCommePassager($this->recupererTrajetsCommePassager());
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
//    public function __toString() : string{
//        return nl2br("nom : $this->nom  \n prenom : $this->prenom  \n login :  $this->login \n ");
//    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur($utilisateurFormatTableau['login'],$utilisateurFormatTableau['nom'],$utilisateurFormatTableau['prenom']);
    }

    public static function recupererUtilisateurs() : array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        $users = array() ;
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $users[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $users;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau) return null;
        else return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES(:login,:nom,:prenom)";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager(): array
    {
        $sql = "SELECT * FROM trajet
                WHERE id IN (
                    SELECT trajetId FROM passager
                    WHERE passagerLogin = '$this->login'
                );";

        $trajets = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        foreach($pdoStatement as $trajet){
            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);
        }

        return $trajets;
    }



}
?>


