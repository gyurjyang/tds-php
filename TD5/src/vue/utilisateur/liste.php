<?php
/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Modele\ModeleUtilisateur;

echo "<h1>Liste des utilisateurs</h1>";
echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<li><p> Utilisateur de login ' . $loginHTML. " " ."<a href='controleurFrontal.php?action=afficherDetail&login=$loginURL'>(+ d'info)</a> </p></li>";
}
echo "</ol>";
echo "<a href='controleurFrontal.php?action=afficherFormulaireCreation'>Créer un utilisateur</a>";
?>