<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if (isset($_GET['login'])) {
            $user = $_GET['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($user);
            if (!isset($utilisateur)) {
                self::afficherErreur("L'utilisateur n'existe pas.");
            }
            else
                self::afficherVue('vueGenerale.php', ["titre" => "Détail de $user", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
        }
        else
            self::afficherErreur("Le login n'a pas été renseigné ! ");

    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de Création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire() : void {
        $user = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($user);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Créé", "utilisateur" => $user->getLogin(), "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function afficherErreur(string $messageErreur): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimerUtilisateur() : void {
        $user = $_GET['login'];
        (new UtilisateurRepository())->supprimer($user);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Supprimé", "utilisateur" => $user, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }

    public static function mettreAJour() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur mis à jour", "utilisateur" => $utilisateur->getLogin(), "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);

    }

    public static function afficherFormulaireMiseAJour() : void {
        $user = $_GET['login'];
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Mise à jour utilisateur", "utilisateur" => $user, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
    }
}
?>