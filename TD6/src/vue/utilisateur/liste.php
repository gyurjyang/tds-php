<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\DataObject\Utilisateur;

echo "<h1>Liste des utilisateurs</h1>";
echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<li><p> Utilisateur de login ' . $loginHTML. " " ."<a href='controleurFrontal.php?action=afficherFormulaireMiseAJour&login=$loginURL'>(= modif)</a> <a href='controleurFrontal.php?action=afficherDetail&login=$loginURL'>(+ d'info)</a> 
<a href='controleurFrontal.php?action=supprimerUtilisateur&login=$loginURL'>(- supp)</a>
</p></li>";
}
echo "</ol>";
echo "<a href='controleurFrontal.php?action=afficherFormulaireCreation'>Créer un utilisateur</a>";
?>