<?php

namespace App\Covoiturage\Modele\Repository;


use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;

class TrajetRepository extends AbstractRepository {

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"]
        );
        $passagers = self::recupererPassagers($trajet);
        $trajet->setPassagers($passagers);
        return $trajet;
    }

    /**
     * @return Trajet[]

    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    } */

    /**
     * @return Utilisateur[]
     */
    private static function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT login FROM passager p
                     JOIN utilisateur u ON  p.passagerLogin = u.login
                     WHERE trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "trajetId" => $trajet->getId(),
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        $lesPassagers = [] ;
        foreach ($pdoStatement as $ligne) {
            $lesPassagers[] = (new UtilisateurRepository())->recupererParClePrimaire($ligne['login']);
        }
        return $lesPassagers;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Trajet $trajet */
        return array(
            "id" => $objet->getId(),
            "depart" => $objet->getDepart(),
            "arrivee" => $objet->getArrivee(),
            "date" => $objet->getDate()->format("Y-m-d"),
            "prix" => $objet->getPrix(),
            "conducteurLogin" => $objet->getConducteur()->getLogin(),
            "nonFumeur" => $objet->isNonFumeur() ? 1 : 0 //si c'est non-fumeur alors c'est 1 sinon c'est 0
        );
    }
}