<?php
namespace App\Covoiturage\Configuration;
class ConfigurationSite {
    public static function verifierDerniereActivite() : void {
        $dureeExpiration = 3600;
        if (isset($_SESSION['derniereActivite']) && (time() - $_SESSION['derniereActivite'] > ($dureeExpiration)))
            session_unset();     // unset $_SESSION variable for the run-time

        $_SESSION['derniereActivite'] = time(); // update last activity time stamp
    }
}