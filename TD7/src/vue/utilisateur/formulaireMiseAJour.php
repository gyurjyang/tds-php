<?php

use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

/** @var string $utilisateur */
$user = (new UtilisateurRepository())->recupererParClePrimaire($utilisateur);
$nomHTML = htmlspecialchars($user->getNom());
$prenomHTML = htmlspecialchars($user->getPrenom());
$loginHTML = htmlspecialchars($user->getLogin());
?>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?=$loginHTML?>" name="login" id="login_id" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?=$prenomHTML?>" name="prenom" id="prenom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?=$nomHTML?>" name="nom" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <input type="submit" value="Modifier" />
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>