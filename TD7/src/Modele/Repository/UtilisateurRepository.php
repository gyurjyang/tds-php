<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository {

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau['login'],$utilisateurFormatTableau['nom'],$utilisateurFormatTableau['prenom']);
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $user): array
    {
        $sql = "SELECT * FROM trajet
                WHERE id IN (
                    SELECT trajetId FROM passager
                    WHERE passagerLogin = :login
                );";

        $trajets = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array("login" => $user->getLogin());
        $pdoStatement->execute($values);
        foreach($pdoStatement as $trajet){
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);
        }

        return $trajets;
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "login" => $utilisateur->getLogin(),
            "nom" => $utilisateur->getNom(),
            "prenom" => $utilisateur->getPrenom()
        );
    }


}