<?php
require "Utilisateur.php";

$trajet = Trajet::recupererTrajetParId($_REQUEST['trajet_id']);
if($trajet->supprimerPassager($_REQUEST['login'])) {
    echo "Le passager {$_REQUEST['login']} a été supprimé du trajet {$_REQUEST['trajet_id']} \n";
} else {
    echo "Le trajet {$_REQUEST['trajet_id']} et le passager {$_REQUEST['login']} ne correspondent pas ! \n";
}