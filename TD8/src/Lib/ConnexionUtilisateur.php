<?php
namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        $session = Session::getInstance();
        $session->enregistrer(self::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        return Session::getInstance()->contient(self::$cleConnexion);
    }

    public static function deconnecter(): void
    {
        Session::getInstance()->supprimer(self::$cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return Session::getInstance()->contient(self::$cleConnexion) ? Session::getInstance()->lire(self::$cleConnexion) : null;
    }

    public static function estUtilisateur($login) : bool {
        return self::estConnecte() && self::getLoginUtilisateurConnecte() === $login;
    }

    public static function estAdministrateur($login) : bool {
        return self::estUtilisateur($login) && ((new UtilisateurRepository())->recupererParClePrimaire($login))->isEstAdmin();
    }
}

