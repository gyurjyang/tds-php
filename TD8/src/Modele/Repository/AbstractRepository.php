<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $liste = $this->getNomsColonnes();
        for ($i = 0; $i < count($liste); $i++) {$liste[$i] = $liste[$i] . "= :" . $liste[$i];}
        $sql = "UPDATE ". $this->getNomTable() . " SET " . join(", ", $liste) .
                " WHERE " . $this->getNomClePrimaire() . "= :". $this->getNomClePrimaire();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
    }
    public function ajouter(AbstractDataObject $objet): bool
    {
        try {
            $sql = "INSERT INTO ".  $this->getNomTable() . " (" . join(",", $this->getNomsColonnes()) . ") VALUES (:" . join(",:", $this->getNomsColonnes()) . ")";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $values = $this->formatTableauSQL($objet);
            $pdoStatement->execute($values);
            return true;
        }
        catch (\PDOException $e) {
            return false;
        }
    }

    public function supprimer(string $valeurClePrimaire): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire()." = :pkTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array("pkTag" => $valeurClePrimaire);
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $pk): ?AbstractDataObject
    {
        $sql = "SELECT * from " . $this->getNomTable() . " WHERE ". $this->getNomClePrimaire() . " = :pkTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "pkTag" => $pk,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau) return null;
        else return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function recuperer(): array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM " . $this->getNomTable();
        $pdoStatement = $pdo->query($sql);
        $users = array();
        foreach ($pdoStatement as $FormatTableau) {
            $users[] = $this->construireDepuisTableauSQL($FormatTableau);
        }
        return $users;
    }

    protected abstract function getNomTable() : string;
    protected abstract function getNomClePrimaire() : string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    /** @return string[] */
    protected abstract function getNomsColonnes() : array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet) : array;


}