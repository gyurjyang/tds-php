<?php

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

/** @var string $utilisateur */
$user = (new UtilisateurRepository())->recupererParClePrimaire($utilisateur);
$nomHTML = htmlspecialchars($user->getNom());
$prenomHTML = htmlspecialchars($user->getPrenom());
$loginHTML = htmlspecialchars($user->getLogin());
$emailHTML = htmlspecialchars($user->getEmail());
$method = ConfigurationSite::getDebug() ? "get" : "post";
?>
<form method=<?=$method?> action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?=$prenomHTML?>" name="prenom" id="prenom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?=$nomHTML?>" name="nom" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?=$loginHTML?>" name="login" id="login_id" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="<?=$emailHTML?>" placeholder="toto@yopmail.com" name="email" id="email_id">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdpActuel">Mot de passe Actuel&#42;</label>
            <input class="InputAddOn-field" type="password" placeholder="" name="mdpActuel" id="mdpActuel" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <?php

        use App\Covoiturage\Lib\ConnexionUtilisateur;
        if (ConnexionUtilisateur::estAdministrateur(ConnexionUtilisateur::getLoginUtilisateurConnecte()))
            {
                $checked = $user->isEstAdmin() ? "checked" : "";
                echo "
                <p class='InputAddOn'>
                <label class='InputAddOn-item' for='estAdmin_id'>Administrateur</label>
                <input class='InputAddOn-field' type='checkbox' placeholder='' name='estAdmin' id='estAdmin_id' $checked></p>";
            }
        ?>

        <p class="InputAddOn">
            <input type="submit" value="Modifier" />
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>