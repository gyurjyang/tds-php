<?php

use App\Covoiturage\Configuration\ConfigurationSite;

$method = ConfigurationSite::getDebug() ? "get" : "post";

?>
<form method=<?=$method?> action="controleurFrontal.php">
    <fieldset>
        <legend>Connexion :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <input type="submit" value="Connexion" />
            <input type='hidden' name='action' value='connecter'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>