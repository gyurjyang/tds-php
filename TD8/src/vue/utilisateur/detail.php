<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

$nomHTML = htmlspecialchars($utilisateur->getNom());
    $prenomHTML = htmlspecialchars($utilisateur->getPrenom());
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<p> Utilisateur de login ' . $loginHTML . '.</p>';
    echo "nom : $nomHTML  <br> prenom : $prenomHTML <br> login : $loginHTML<br> ";
    if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
        echo "<a href='controleurFrontal.php?action=afficherFormulaireMiseAJour&login=$loginURL&controleur=utilisateur'>(= modif)</a>";
        echo "<a href='controleurFrontal.php?action=supprimerUtilisateur&login=$loginURL&controleur=utilisateur'>(- supp)</a>";
    }
