<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if (isset($_REQUEST['login'])) {
            $user = $_REQUEST['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($user);
            if (!isset($utilisateur)) {
                self::afficherErreur("L'utilisateur n'existe pas.");
            }
            else
                self::afficherVue('vueGenerale.php', ["titre" => "Détail de $user", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
        }
        else
            self::afficherErreur("Le login n'a pas été renseigné ! ");

    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de Création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        if ($_REQUEST["mdp"] != $_REQUEST["mdp2"]) {
            self::afficherErreur("Mot de passe distinct");
        }
        else {
            if (!filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL)) {
                self::afficherErreur("Email invalide");
            }
            $user = self::construireDepuisFormulaire($_REQUEST);
            if ((new UtilisateurRepository())->recupererParClePrimaire($user->getLogin()) !== null) {self::afficherErreur("L'utilisateur existe déjà");}
            else {
                if (!ConnexionUtilisateur::estAdministrateur($user->getLogin())) {
                    $user->setEstAdmin(0);
                }
                (new UtilisateurRepository())->ajouter($user);
                VerificationEmail::envoiEmailValidation($user);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Créé", "utilisateur" => $user->getLogin(), "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
            }
        }
    }

    public static function validerEmail() : void {
        $login = $_REQUEST["login"] ?? null;
        $nonce = $_REQUEST["nonce"] ?? null;
        if ($login != null && $nonce != null) {
            $verif = VerificationEmail::traiterEmailValidation($login, $nonce);
            $verif ? self::afficherDetail() : self::afficherErreur("Erreur lors de la vérification");
        }
        else {
            self::afficherErreur("Erreur : données manquantes");
        }
    }

    public static function afficherErreur(string $messageErreur): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimerUtilisateur() : void {
        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            self::afficherErreur("Erreur : l'utilisateur supprimé n'est pas l'utilisateur connecté");
            return;
        }
        if ((new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) == null) {
            self::afficherErreur("Erreur : l'utilisateur n'existe pas !");
            return;
        }
        $user = $_REQUEST['login'];
        ConnexionUtilisateur::deconnecter();
        (new UtilisateurRepository())->supprimer($user);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Supprimé", "utilisateur" => $user, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }

    public static function mettreAJour() : void {
        $userAdminExiste = ConnexionUtilisateur::estAdministrateur(ConnexionUtilisateur::getLoginUtilisateurConnecte()) && (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) != null;
        $userConnecte = $_REQUEST['login'] == ConnexionUtilisateur::getLoginUtilisateurConnecte();
        if ($userAdminExiste || $userConnecte) {
            if ((new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) == null) {
                self::afficherErreur("Login inconnu");
                return;
            }
            if (!$userAdminExiste) {
                $mdpUser = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login'])->getMdpHache();
                if(!MotDePasse::verifier($_REQUEST['mdpActuel'], $mdpUser)) {
                    self::afficherErreur("Mot de passe actuel incorrect");
                    return;
                }
            }
            if (!isset($_REQUEST['nom']) || !isset($_REQUEST['mdp']) || !isset($_REQUEST['mdp2']) || !isset($_REQUEST['mdpActuel']) || !isset($_REQUEST['prenom'])) {
                self::afficherErreur("Erreur : tous les champs ne sont pas complétés !");
                return;
            }
            if($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
                self::afficherErreur("Erreur : les deux mots de passe ne coincident pas");
                return;
            }
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            $utilisateur->setNom($_REQUEST['nom']);
            $utilisateur->setPrenom($_REQUEST['prenom']);
            $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
            if ($utilisateur->getEmail() != $_REQUEST['email']) {
                if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                    self::afficherErreur("Email invalide");
                }
                $utilisateur->setEmailAValider($_REQUEST['email']);
                $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
                VerificationEmail::envoiEmailValidation($utilisateur);
            }
            if ($userAdminExiste) $utilisateur->setEstAdmin(isset($_REQUEST['estAdmin']));
            (new UtilisateurRepository())->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur mis à jour", "utilisateur" => $utilisateur->getLogin(), "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
        }
        else {
            self::afficherErreur("Erreur : l'utilisateur mis à jour n'est pas l'utilisateur connecté");
        }
    }

    public static function afficherFormulaireMiseAJour() : void {
        $userAdminExiste = ConnexionUtilisateur::estAdministrateur(ConnexionUtilisateur::getLoginUtilisateurConnecte()) && (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) != null;
        $userConnecte = $_REQUEST['login'] == ConnexionUtilisateur::getLoginUtilisateurConnecte();
        if ($userAdminExiste or $userConnecte) {
            $user = $_REQUEST['login'];
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Mise à jour utilisateur", "utilisateur" => $user, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
        }
        else if (ConnexionUtilisateur::estAdministrateur(ConnexionUtilisateur::getLoginUtilisateurConnecte()) && (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) == null) {
            self::afficherErreur("Login inconnu");
        }
        else {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
        }
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $estAdmin = isset($tableauDonneesFormulaire['estAdmin']) ? 1 : 0;
        return new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom'], MotDePasse::hacher($tableauDonneesFormulaire['mdp']), $estAdmin, "", $tableauDonneesFormulaire['email'], MotDePasse::genererChaineAleatoire());
    }

    public static function afficherFormulaireConnexion() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter() : void {
        if (isset($_REQUEST['login']) && isset($_REQUEST['mdp'])) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            if($utilisateur !== null) {
                if (VerificationEmail::aValideEmail($utilisateur)) {
                    if (MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache())) {
                        ConnexionUtilisateur::connecter($utilisateur->getLogin());
                        self::afficherVue('vueGenerale.php', ["titre" => "Connecté !", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "utilisateur" => $utilisateur]);
                    }
                    else {
                        self::afficherErreur("Login et/ou mot de passe incorrect");
                    }
                }
                else {
                    self::afficherErreur("Veuillez vérifier votre email !");
                }
            }
            else {
                self::afficherErreur("Login invalide");
            }
        }
        else {
            self::afficherErreur("Login et/ou mot de passe manquant");
        }
    }

    public static function deconnecter() : void {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Déconnecté !", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php", "utilisateurs" => $utilisateurs]);
    }

/*    public static function deposerCookie() : void {
        Cookie::enregistrer("attribut" ,1 , 30);
    }

    public static function lireCookie() : void
    {
        Cookie::lire("attribut");
    }

    public static function supprimerCookie() : void
    {
        Cookie::supprimer("attribut");
    }*/

    public static function testSession() : void {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Cathy Penneflamme");
        $session->enregistrer("numTel", array(48484848,4848484654,858545));
        var_dump($_SESSION);
    }
    public static function suppSession() : void {
        Session::getInstance()->detruire();
    }
}